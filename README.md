# What's in this

A telegram bot capable of executing normal commands and "talk like" commands.
By now "talking" is allowed only in private chats, not in grops.
**You can still send and receive messages from groups, but you can't save a state** 

## What is implemented

- [x] Text commands
- [x] Dialog commands
- [x] Send message api
    - [x] inline markup keyboard
    - [x] answering keyboard (by Reply keyboard (one time only)
- [x] Edit messages
- [x] Callback answer
- [x] Callbacks

## What's not

- [ ] Payments
- [ ] Answerng messages

# Commands

- **/start** `Starts the bot and registers the user if not already in the database`
- **/start** in a group sends a description and registers the chat id in the database alongside the title
    - **chat name changed callback** updating current chat name in the database
- **/event** `Publish an event message in a group`
    - callback for event subscription
- **/purge** Removes user data from the database

## TODO

- **/close** Only for groups, removes the chat entry from the db
- **/edit** Edits current user data _Dunno if needed_
- **/list** list event subscribers
