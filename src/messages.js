export const benvenuto_guest = 
`Benvenuto! 🤗
Non mi hai mai scritto, quindi é opportuno che tu mi dica qualche informazione sul tuo conto, al fine di agevolare le operazioni di iscrizione alle gare.`;
export const benvenuto_gdpr = 
`Utilizzando questo bot, acconsenti all'utilizzo dei tuoi dati (anno di nascita e peso) al fine di agevolare le procedure di iscrizione alle competizioni proposte.
I dati relativi alla tua persona saranno salvati e persisteranno anche a competizione avvenuta.
Tutte le informazioni sono salvate senza riportare il tuo nome, ma solamente il tuo id del profilo Telegram. Il tuo nome apparitá solo nel momento dell'iscrizione ad un evento.
Potrai inoltre richiedere in ogni momento una copia dei tuoi dati personali contattando l'indirizzo email \`info@jcc-judo.it\`, inoltre potrai, in qualsiasi momento, richiedere la cancellazione dei tuoi dati mediante il comando \`/purge\`.

*Acconsenti al trattamento dei tuoi dati?*`;

export const benvenuto_group = 
`Buongiorno a tutti!
Questo messaggio é stato inviato da un sistema automatico per la gestione delle competizioni.
D'ora in poi, quando verrá proposto un evento in palestra, un messaggio verrá inviato su questo gruppo con al seguito l'opzione (aperta a tutti i partecipanti) di iscriversi telematicamente.
Questo permetterá di agevolare le iscrizioni agli eventi.
Per usufruire del servizio é necessario iniziare una conversazione con me @judo_club_capelletti_bot e utilizzate il comando \`/start\` nella chat!`;
export const benvenuto_guest_registred = "Bentornato! Sei giá registrato.";
export const benvenuto_admin_registred = `Hola, admin! ${["😀", "😁", "😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊", "😋", "😎", "😍", "😘", "🥰", "😗", "😙", "😚", "☺️", "🙂", "🤗", "🤩", "🤔", "🤨", "😐", "😑", "😶", "🙄", "😏", "😣", "😥", "😮", "🤐", "😯", "😪", "😫", "😴", "😌", "😛", "😜", "😝", "🤤", "😒", "😓", "😔", "😕", "🙃", "🤑", "😲", "☹️", "🙁", "😖", "😞", "😟", "😤", "😢", "😭", "😦", "😧", "😨", "😩", "🤯", "😬", "😰", "😱", "🥵", "🥶", "😳", "🤪", "😵", "😡", "😠", "🤬", "😷", "🤒", "🤕", "🤢", "🤮", "🤧", "😇", "🤠", "🤡", "🥳", "🥴", "🥺", "🤥", "🤫", "🤭", "🧐", "🤓", "😈", "👿", "👹", "👺", "💀", "👻", "👽", "🤖", "💩", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾"][Math.floor(Math.random()*107)]}`;

export const ask_year = "In che anno sei nato?";
export const ask_weight = "Qual'é la tua categria di peso? ti consiglierei di indicarla senza unitá di misura";

export const error_year = "Che anno é? Non sono in grado di decifrarlo 😡";
export const error_weight = "Che peso é? Non sono in grado di decifrarlo 😡";
export const error_not_admin = "Ahimé non sei un amministratore ☹️";

export const registration_ok = 
`Perfetto! 😁
Ti sei registrato con successo.
Riordati che puoi chiedere in ogni momento la cancellazione dei tuoi dati dal database con \`/purge\``;
export const registration_fail = "Qualcosa é andato storto, qualcuno avrebbe detto\n *My batteries are low and it's getting dark.*";

export const event_introduction = "Ottimo, Che titolo vuoi dargli? (18 caratteri)";
export const event_get_description = "Descrizione?";
export const event_get_date = "Una data please (gg/mm/aaaa HH:MM)";
export const event_get_date_error = "Controla la sintassi perfavore! 😳";
export const event_get_chat = "In che gruppo vuoi pubblicare?";
export const event_aborted = "Va bene, tutto come prima! 🙃";
export const event_make = (title, description, date, iscritti) => `*--------------------*\n| C'é una nuova proposta!\n|*${title}*\n|*${description}*\n|\n|_${date}_\n|Attualmente *${iscritti}* iscritti\n*--------------------*`;
export const event_make_preview = (text) => `*=========PREVIEW MESSAGE=========*\n${text}\n*=========PREVIEW MESSAGE=========*\nVa bene?`;

export const cb_not_registred = "Non sei registrato 😲";
export const cb_error = "É successo qualcosa di MOOOLTO strano, non posso continuare.";
export const cb_ok = "Ottimo, ti aspettiamo!";

export const purge_sure = "Con questo comando cancellerai ogni dato relativo a te, il tuo stato, i tuoi dati e le tue iscrizioni. Sei sicuro?";
export const purge_ok = "Tu non esisti piú. Esegui start nuovamente per ricrearti.";
export const purge_error = "Non mi é possibile esaudire i Vostri desideri, contatta Stefano.";

export const list_choose = "Quale evento?";
export const list_subscriptions = (event, users) =>
    `Lista di iscritti a ${event}:\n\n
NOME---------------+ANNO+CAT+
${users.map((e, i) => `*${e.name}* ${(i % 2 === 0 ? '-' : ' ').repeat(20 - e.name.length)} ${e.year}   ${e.cat}\n`)}`;
