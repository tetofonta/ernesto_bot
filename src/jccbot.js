import Bot from "./bot/Bot";
import start from "./commands/start";
import {callback as event_callback_sub, default as event} from "./commands/event";
import * as faunadb from "faunadb";
import purge from "./commands/purge";
import list from "./commands/list";

const q = faunadb.query;


export function handler(evt, ctx, reply) {
    try {
        new Bot(process.env.TELEGRAM_TOKEN, process.env.FAUNA_DB_KEY)
            .onCommandFromObject(start())
            .onCommandFromObject(event())
            .onCommandFromObject(list())
            .onCommandFromObject(purge())
            .fallbackMessage(async (env, message, db) => {
                if (message.new_chat_title) {
                    let r = await db.query(q.Get(q.Match(q.Index("registered_chats_by_chat_id"), message.chat.id)));
                    await db.query(q.Update(q.Ref(q.Collection("registered_chats"), r.ref.id), {data: {title: message.new_chat_title}}))
                }
            })
            .onCallback(async (env, qry, data, db) => {
                qry.data = JSON.parse(qry.data);
                if (qry.data.action === "sub") {
                    return await event_callback_sub(env, qry, data, db)
                }
            })
            .do(JSON.parse(evt.body))
            .then(_ => reply(null, {statusCode: 204}))
            .catch(_ => reply(process.env.env === "development" ? _ : "BotError", {statusCode: 500}));
    } catch (e) {
        reply(process.env.env === "development" ? e : "BotError", {statusCode: 500})
    }
}