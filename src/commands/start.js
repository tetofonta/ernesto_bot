import {
    ask_weight,
    ask_year,
    benvenuto_admin_registred,
    benvenuto_gdpr,
    benvenuto_group,
    benvenuto_guest,
    benvenuto_guest_registred,
    error_weight,
    error_year,
    registration_fail,
    registration_ok
} from "../messages";
import MessageAction from "../bot/Message/MessageAction";
import * as faunadb from "faunadb";
import ReplyKeyboard from "../bot/Message/ReplyKeyboard";

const q = faunadb.query;


export default function () {
    const command = "start";

    const init = async (env, msg, data, abort, next, db) => {
        if(data.from === "group"){

            try{
                await db.query(q.Create(q.Collection("registered_chats"), {data: {chat_id: msg.chat.id, title: msg.chat.title}}));
            } catch (e){}
            await env.message(msg.chat.id, benvenuto_group, new MessageAction().parse());

        } else if (data.isNew) {

            await env.message(msg.chat.id, benvenuto_guest, new MessageAction().parse());
            await env.message(msg.chat.id, benvenuto_gdpr, new MessageAction().parse().reply_markup(new ReplyKeyboard().addButton("Confermo", 0, 0).addButton("NO!", 1, 0)));
            await next({}, "gdpr");

        } else {

            if (data.isAdmin) await env.message(msg.chat.id, benvenuto_admin_registred, new MessageAction().parse());
            else await env.message(msg.chat.id, benvenuto_guest_registred, new MessageAction().parse());
            await abort();

        }
    };

    const steps = {
        "gdpr": async (env, msg, data, abort, next, db) => {
            if(msg.text === "Confermo"){
                await db.query(q.Update(q.Ref(q.Collection("user_states"), data.ref_id), {
                    data: {
                        gdpr: true
                    }
                }));
                await env.message(msg.chat.id, ask_year, new MessageAction().parse());
                await next({}, "ask_year");
            } else {
                await env.message(msg.chat.id, benvenuto_gdpr, new MessageAction().parse().reply_markup(new ReplyKeyboard().addButton("Confermo", 0, 0).addButton("NO!", 1, 0)));
                await next({}, "gdpr");
            }
        },
        "ask_year": async (env, msg, data, abort, next, db) => {
            let year = parseInt(msg.text);
            if (isNaN(year)) {
                await env.message(msg.chat.id, error_year, new MessageAction().parse());
                await next({}, "ask_year");
                return;
            }

            await env.message(msg.chat.id, ask_weight, new MessageAction().parse());
            await next(Object.assign({year}, JSON.parse(data.state_data)), "ask_weight");
        },
        "ask_weight": async (env, msg, data, abort, next, db) => {
            let weight = parseInt(msg.text);
            if (isNaN(weight)) {
                await env.message(msg.chat.id, error_weight, new MessageAction().parse());
                await next(JSON.parse(data.state_data), "ask_weight");
                return;
            }

            try {
                await db.query(q.Create(q.Collection("user_data"), {
                    data: {
                        id: data.id,
                        year: JSON.parse(data.state_data).year,
                        weight
                    }
                }));
                await env.message(msg.chat.id, registration_ok, new MessageAction().parse());
            } catch (e) {
                await env.message(msg.chat.id, registration_fail, new MessageAction().parse());
                throw Error("Cannot save the data")
            }
            await abort();
        }
    };

    return {command, init, steps, groups: true, clients: true}
}