import {
    cb_error,
    cb_not_registred,
    cb_ok,
    error_not_admin,
    event_aborted,
    event_get_chat,
    event_get_date,
    event_get_date_error,
    event_get_description,
    event_introduction,
    event_make,
    event_make_preview,
    registration_fail
} from "../messages";
import MessageAction from "../bot/Message/MessageAction";
import * as faunadb from "faunadb";
import ReplyKeyboard from "../bot/Message/ReplyKeyboard";
import InlineKeyboardMarkup from "../bot/Message/InlineKeyboardMarkup";

const q = faunadb.query;


export default function () {
    const command = "event";

    const init = async (env, msg, data, abort, next, db) => {
        if (data.isAdmin) {
            await env.message(msg.chat.id, event_introduction, new MessageAction().parse());
            await next({}, "get_title")
        } else {
            await env.message(msg.chat.id, error_not_admin, new MessageAction().parse());
            await abort();
        }
    };

    const steps = {
        "get_title": async (env, msg, data, abort, next, db) => {
            let title = msg.text;
            await env.message(msg.chat.id, event_get_description, new MessageAction().parse());
            await next({title}, "get_description");
        },
        "get_description": async (env, msg, data, abort, next, db) => {
            let desc = msg.text;
            await env.message(msg.chat.id, event_get_date, new MessageAction().parse());
            await next(Object.assign({desc}, JSON.parse(data.state_data)), "get_date");
        },
        "get_date": async (env, msg, data, abort, next, db) => {
            let date_str = msg.text.trim().replace(/\//g, " ").replace(/:/g, " ").split(" ");
            let maxd = 0;
            let ok = true;

            if (parseInt(date_str[1]) > 12 || parseInt(date_str[1]) < 1 || isNaN(parseInt(date_str[1]))) ok = ok && false;
            switch (parseInt(date_str[1])) { //month
                default:
                    maxd = 31;
                    break;
                case 11:
                case 4:
                case 6:
                case 9:
                    maxd = 30;
                    break;
                case 2:
                    maxd = 28 + (parseInt(date_str)[2] % 4 ? 1 : 0);
            }

            if (parseInt(date_str[0]) > maxd || parseInt(date_str[0]) < 1 || isNaN(parseInt(date_str[0]))) ok = ok && false;
            if (parseInt(date_str[2]) < 1990 || isNaN(parseInt(date_str[2]))) ok = ok && false;
            if (parseInt(date_str[3]) > 24 || parseInt(date_str[0]) < 0 || isNaN(parseInt(date_str[3]))) ok = ok && false;
            if (parseInt(date_str[4]) > 59 || parseInt(date_str[4]) < 0 || isNaN(parseInt(date_str[4]))) ok = ok && false;

            if (!ok) {
                await env.message(msg.chat.id, event_get_date_error, new MessageAction().parse());
                await next(JSON.parse(data.state_data), "get_date");
            } else {
                // let chats = await db.query(q.Paginate(q.Match(q.Index("all_registered_chats"))));
                // chats.data = await Promise.all(chats.data.map(async (e) => await db.query(q.Get(q.Ref(q.Collection("registered_chats"), e.id)))));

                let chats = await db.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("all_registered_chats"))),
                        q.Lambda('ref',
                            q.Get(q.Var('ref')))
                    )
                );

                await env.message(msg.chat.id, event_get_chat,
                    new MessageAction().parse().reply_markup(
                        chats.data.reduce((o, c, i) => o.addButton(c.data.title, i, 0), new ReplyKeyboard())
                    ));
                await next(Object.assign({date: new Date(`${date_str[1]}/${date_str[0]}/${date_str[2]} ${date_str[3]}:${date_str[4]}:00Z`).toUTCString()}, JSON.parse(data.state_data)), "select_publishing_chat");
            }
        },
        "select_publishing_chat": async (env, msg, data, abort, next, db) => {
            let chat = msg.text;
            let event_data = JSON.parse(data.state_data);
            await env.message(msg.chat.id, event_make_preview(event_make(event_data.title, event_data.desc, event_data.date, "0")),
                new MessageAction()
                    .parse()
                    .reply_markup(
                        new ReplyKeyboard().addButton("Conferma", 0, 0).addButton("Annulla", 0, 1)
                    )
            );
            await next(Object.assign({chat}, event_data), "confirm")
        },
        "confirm": async (env, msg, data, abort, next, db) => {
            if (msg.text === "Conferma") {
                let event_data = JSON.parse(data.state_data);
                let chat = await db.query(q.Get(q.Match(q.Index("registered_chats_by_chat_title"), event_data.chat)));

                let finish_date = new Date(event_data.date);
                finish_date.setHours(finish_date.getHours() + 3);

                let message = await env.message(chat.data.chat_id, event_make(event_data.title, event_data.desc, event_data.date, "0"),
                    new MessageAction()
                        .parse()
                        .reply_markup(
                            new InlineKeyboardMarkup()
                                .addCallbackButton("Ci sono!", 0, 0, {
                                    action: "sub",
                                    event_title: event_data.title,
                                    subs: 0
                                })
                                .addURLButton("Aggiungilo al mio calendario", 1, 0, encodeURI(`http://www.google.com/calendar/event?action=TEMPLATE&dates=${new Date(event_data.date).toISOString().replace(/-/g, "").replace(/:/g, "").replace(/.000/g, "")
                                    }/${
                                        finish_date.toISOString()
                                            .replace(/-/g, "")
                                            .replace(/:/g, "")
                                            .replace(/.000/g, "")
                                    }&text=${event_data.title}&details=${event_data.desc}`)
                                )
                        )
                );

                try {
                    await db.query(q.Create(q.Collection("published_events"), {
                        data: {
                            title: event_data.title,
                            description: event_data.desc,
                            date: event_data.date,
                            chat_id: chat.data.chat_id,
                            message_id: message.message_id,
                            subs: 0
                        }
                    }));
                } catch (e) {
                    await env.message(msg.chat.id, registration_fail, new MessageAction().parse());
                }

            } else {
                await env.message(msg.chat.id, event_aborted, new MessageAction().parse());
            }
            await abort();
        }
    };

    return {command, init, steps, groups: false, clients: true}
}

export async function callback(env, qry, data, db) {
    let from = qry.from.first_name + " " + qry.from.last_name;
    //get user infos
    try {
        await db.query(q.Get(q.Match(q.Index("user_data_by_user_id"), data.id)));
    } catch (e) {
        return {id: qry.id, text: cb_not_registred, alert: false}
    }
    try {
        await db.query(q.Create(q.Collection("event_subs"), {
            data: {
                user_id: parseInt(qry.from.id),
                event_title: qry.data.event_title,
                user_name: from
            }
        }));
        let event = await db.query(q.Get(q.Match(q.Index("published_events_by_title"), qry.data.event_title)));
        //todo calculate from db subs
        let finish_date = new Date(event.data.date);
        finish_date.setHours(finish_date.getHours() + 3);
        await env.edit_message(qry.message.chat.id, qry.message.message_id, event_make(event.data.title, event.data.description, event.data.date, qry.data.subs + 1),
            new MessageAction()
                .parse()
                .reply_markup(
                    new InlineKeyboardMarkup()
                        .addCallbackButton("Ci sono!", 0, 0, {
                            action: "sub",
                            event_title: event.data.title,
                            subs: qry.data.subs + 1
                        })
                        .addURLButton("Aggiungilo al mio calendario", 1, 0, encodeURI(`http://www.google.com/calendar/event?action=TEMPLATE&dates=${new Date(event.data.date).toISOString().replace(/-/g, "").replace(/:/g, "").replace(/.000/g, "")
                            }/${
                                finish_date.toISOString()
                                    .replace(/-/g, "")
                                    .replace(/:/g, "")
                                    .replace(/.000/g, "")
                            }&text=${event.data.title}&details=${event.data.description}`)
                        )
                ).get()
        );
        return {id: qry.id, text: cb_ok, alert: false}
    } catch (e) {
        console.log(e);
        return {id: qry.id, text: cb_error, alert: false}
    }
}