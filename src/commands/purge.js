import {purge_error, purge_ok, purge_sure} from "../messages";
import MessageAction from "../bot/Message/MessageAction";
import * as faunadb from "faunadb";
import ReplyKeyboard from "../bot/Message/ReplyKeyboard";

const q = faunadb.query;


export default function () {
    const command = "purge";

    const init = async (env, msg, data, abort, next, db) => {
        await env.message(msg.chat.id, purge_sure, new MessageAction().parse().reply_markup(new ReplyKeyboard().addButton("Conferma", 0, 0).addButton("No", 0, 1)));
        await next({}, "sure")
    };

    const steps = {
        "sure": async (env, msg, data, abort, next, db) => {
            if (msg.text === "Conferma") {
                try {
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_states_by_user_id"), msg.from.id)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_data_by_user_id"), msg.from.id)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("event_subs_by_user_id"), msg.from.id)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_states_by_user_id"), msg.from.id)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await env.message(msg.chat.id, purge_ok, new MessageAction().parse())
                } catch (e) {
                    await env.message(msg.chat.id, purge_error, new MessageAction().parse())
                }
            }
            await abort();
        }
    };

    return {command, init, steps, groups: false, clients: true}
}