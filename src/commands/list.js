import * as faunadb from "faunadb";
import {error_not_admin, list_choose, list_subscriptions} from "../messages";
import MessageAction from "../bot/Message/MessageAction";
import ReplyKeyboard from "../bot/Message/ReplyKeyboard";

const q = faunadb.query;


export default function () {
    const command = "list";

    const init = async (env, msg, data, abort, next, db) => {
        if (data.isAdmin) {
            let events = await db.query(
                q.Map(
                    q.Paginate(q.Match(q.Index("all_published_events"))),
                    q.Lambda('ref', q.Get(q.Var('ref')))
                )
            );
            let curDate = new Date();
            await env.message(msg.chat.id, list_choose, new MessageAction().parse().reply_markup(
                events.data.filter(e => new Date(e.data.date) > curDate).reduce((o, d, i) => o.addButton(d.data.title, i, 0), new ReplyKeyboard())
            ));
            await next({}, "quale");
            return;
        }

        await env.message(msg.chat.id, error_not_admin, new MessageAction().parse());
        await abort();
    };

    const steps = {
        "quale": async (env, msg, data, abort, next, db) => {
            let event_title = msg.text;

            let users = await db.query(
                q.Map(
                    q.Paginate(q.Match(q.Index("event_subs_by_title"), event_title)),
                    q.Lambda(
                        'ref',
                        q.Get(q.Var('ref'))
                    )
                )
            );

            users = await Promise.all(users.data.map(async u => {
                let nfo = await db.query(q.Get(q.Match(q.Index('user_data_by_user_id'), parseInt(u.data.user_id))));
                return {
                    name: u.data.user_name,
                    year: nfo.data.year,
                    cat: nfo.data.weight
                }
            }));

            await env.message(msg.chat.id, list_subscriptions(event_title, users), new MessageAction().parse());

            await abort();
        }
    };

    return {command, init, steps, groups: false, clients: true}
}
