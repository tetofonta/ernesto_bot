import * as faunadb from "faunadb"
import https from "https"

const q = faunadb.query;

export default class Bot {

    constructor(token, secret) {
        this.token = token;
        this.db = new faunadb.Client({secret});

        this.commands = {};
        this.onFallbackMessage = () => {
        };
        this.cb = () => {
        }
    }

    fallbackMessage(fnc) {
        this.onFallbackMessage = fnc;
        return this;
    }

    onCommand(command, init, steps, groups, clients) {
        this.commands[command] = {init, steps, groups, clients};
        return this;
    }

    onCommandFromObject(data) {
        return this.onCommand(data.command, data.init, data.steps, data.groups, data.clients)
    }

    onCallback(fnc) {
        this.cb = fnc;
        return this;
    }

    async abort(id) {
        await this.db.query(q.Update(q.Ref(q.Collection("user_states"), "" + id), {
            data: {
                current_command: null,
                current_state: null
            }
        }))
    }

    async next(id, cmd, current_state, data, next_status) {
        if (!next_status) {
            //if status has not been specified get next status from list or abort if last one
            let current_status_index = Object.keys(this.commands[cmd].steps).indexOf(current_state);
            if (current_status_index === Object.keys(this.commands[cmd].steps).length - 1) return await this.abort(id);

            next_status = Object.keys(this.commands[cmd].steps)[current_status_index + 1]
        }

        await this.db.query(q.Update(q.Ref(q.Collection("user_states"), id), {
            data: {
                current_state: next_status,
                state_data: JSON.stringify(data)
            }
        }))
    }

    async fetchUserData(userid) {
        try {
            //try fetch already existing data, throw if not available
            let data = await this.db.query(q.Get(q.Match(q.Index("user_states_by_user_id"), userid)));
            data.data.ref_id = data.ref.id;
            return {ref_id: data.ref.id, data: data.data};
        } catch (e) {
            //data was not available, create new user
            let data = {
                "id": userid,
                "current_command": null,
                "current_state": null,
                "state_data": "{}"
            };
            data = await this.db.query(q.Create(q.Collection("user_states"), {data}));
            data.data.isNew = true;
            data.data.ref_id = data.ref.id;
            return {ref_id: data.ref.id, data: data.data}
        }
    }

    async do(update) {
        if (update.message && update.message.from) return await this.onMessage(update.message);
        if (update.callback_query) return await this.onCallbackData(update.callback_query);
        throw Error("No message nor callback")
    }

    async onMessage(message) {
        //fetch user state
        let data = await this.fetchUserData(message.from.id);
        let ref_id = data.ref_id;
        data = data.data;

        if (message.chat.id < 0) //itsagroup
            data.from = "group";
        else
            data.from = "client";

        let command = message.text ? message.text.split(" ")[0].split("@")[0].substr(1) : undefined;

        if (this.commands[command] && !data.current_command) {
            if (!this.commands[command].groups && data.from === "group") throw Error("Sending message from a group not allowesd");
            if (!this.commands[command].clients && data.from === "client") throw Error("client message not allowed");

            if (data.from !== "group") await this.db.query(q.Update(q.Ref(q.Collection("user_states"), ref_id), {data: {current_command: command}}));
            //call command init fnc
            await this.commands[command].init(this, message, data, () => this.abort(ref_id), (new_data = {}, step = undefined) => this.next(ref_id, command, undefined, new_data, step), this.db);

            if (this.commands[command].steps.length === 0) //if command has no steps, abort (return to no state/command status)
                await this.abort(ref_id);
        } else {
            //if a command was running or no command has been recognized
            if (message.text && message.text.split(" ")[0].split("@")[0].substr(1) === "abort")  //if new text is /abort or /abort@bot abort command exec
                await this.abort(ref_id);
            else if (!this.commands[data.current_command] || !this.commands[data.current_command].steps[data.current_state])
                await this.onFallbackMessage(this, message, this.db);
            else await this.commands[data.current_command].steps[data.current_state](
                    this,
                    message,
                    data,
                    () => this.abort(ref_id),
                    (new_data = {}, step = undefined) => this.next(ref_id, data.current_command, data.current_state, new_data, step),
                    this.db
                );
        }
    }

    async onCallbackData(cbq) {
        let data = await this.fetchUserData(cbq.from.id);
        let ref_id = data.ref_id;
        data = data.data;

        let answ = await this.cb(this, cbq, data, this.db);
        await this.api_request("answerCallbackQuery", {callback_query_id: answ.id, text: answ.text, show_alert: answ.alert})
    }

    //api calls
    api_request(method, body) {
        return new Promise((resolve, reject) => {
            const data = JSON.stringify(body) + " ";

            const req = https.request({
                hostname: process.env.env === "development" ? "localhost" : 'api.telegram.org',
                port: process.env.env === "development" ? 4433 : 443,
                path: `/bot${this.token}/${method}`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': data.length
                }
            }, (res) => {
                let data = "";
                res.on('data', (d) => {
                    data += d.toString();
                });
                res.on('end', () => {
                    if (res.statusCode !== 200) {
                        console.log(data);
                        reject("Status code returned not ok");
                    } else resolve(JSON.parse(data))
                })
            });

            req.on('error', (error) => {
                reject(error)
            });

            req.write(data);
            req.end()
        })
    }

    async message(to, text, action) {
        return await this.api_request("sendMessage", Object.assign({
            chat_id: to,
            text
        }, action.get()))
    }

    async edit_message(chat_id, message_id, text, options) {
        return await this.api_request("editMessageText", Object.assign({chat_id, message_id, text}, options))
    }

    async answer_callback_query(callback_query_id, text, show_alert = false, url = undefined) {
        return await this.api_request("answerCallbackQuery", {callback_query_id, text, show_alert, url})
    }

}