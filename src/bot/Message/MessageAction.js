export default class{

    constructor(){
        this.baseObject = {}
    }

    reply(message_id){
        this.baseObject.reply_to_message_id = message_id;
        return this;
    }

    parse(as="Markdown"){
        this.baseObject.parse_mode = as;
        return this;
    }

    other(key, value){
        this.baseObject[key] = value;
        return this;
    }

    reply_markup(markup){
        this.baseObject.reply_markup = markup.get()
        return this;
    }

    get(){
        return this.baseObject;
    }
}