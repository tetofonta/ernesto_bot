export default class{

    constructor(){
        this.baseObject = {inline_keyboard: []}
    }

    get(){
        return JSON.stringify(this.baseObject);
    }

    addButton(text, row, col, data){
        if(!this.baseObject.inline_keyboard[row]) this.baseObject.inline_keyboard[row] = [];
        this.baseObject.inline_keyboard[row][col] = Object.assign({text}, data)
        return this;
    }

    addCallbackButton(text, row, col, cb_data){
        return this.addButton(text, row, col, {callback_data: JSON.stringify(cb_data)})
    }

    addURLButton(text, row, col, url){
        return this.addButton(text, row, col, {url})
    }

    addLoginButton(text, row, col, url, fwtext){
        return this.addButton(text, row, col, {login_url: {url, forward_text: fwtext}})
    }
}