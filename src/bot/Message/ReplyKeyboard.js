export default class{

    constructor(){
        this.baseObject = {keyboard: [], one_time_keyboard: true}
    }

    get(){
        return JSON.stringify(this.baseObject);
    }

    addButton(text, row, col){
        if(!this.baseObject.keyboard[row]) this.baseObject.keyboard[row] = [];
        this.baseObject.keyboard[row][col] = {text};
        return this;
    }
}