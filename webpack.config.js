const webpack = require("webpack");
const path = require("path");
const cfg = require("./package");

module.exports = {
    mode: process.env.env || "production",
    entry: path.resolve(__dirname, cfg.lambda_entry),
    output: {
        path: path.resolve(__dirname, "./functions"),
        filename: "jccbot.js",
        library: "ernesto_capelletti_bot",
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    target: "node",
    node: {
        __dirname: true,
    },
    plugins: [
        new webpack.DefinePlugin({ "global.GENTLY": false })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
            }
        ]
    },
};