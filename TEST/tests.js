describe("ERNESTO's TESTS", function(){

    before(function () {
        Object.assign(process.env, require("../env_keys"));
    });

    require("./structure.test");
    // require("./database.test");
    require("./bot.test")
});
