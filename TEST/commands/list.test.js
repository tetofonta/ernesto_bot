const faunadb = require("faunadb");
const assert = require("assert");
const {send_message, telegram_api_mock, telegram_api_mock_end, wait_message, empty_queue} = require("../bot_helper");
const msg = require("../../lib/messages");
const q = faunadb.query;

describe("/list", function () {

    let client;
    let handler;
    let mock;
    before(function () {
        handler = require("../../functions/jccbot.js").handler;
        client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});
        mock = telegram_api_mock();
    });

    after(function () {
        telegram_api_mock_end()
    });

    describe('Normal execution', function () {
        describe('Existent user', function () {
            it("Should ask for event and reply with available events", function (done) {
                send_message(handler, "/list", 123456789, 342823053)
                    .then(_ => wait_message())
                    .then(ob => {
                        console.log(ob)
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            it("should send sub list", function (done) {
                send_message(handler, "ThisIsASmallEvent", 123456789, 342823053)
                    .then(_ => wait_message())
                    .then(ob => {
                        console.log(ob)
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            before(async function () {

            });

            after(async function () {

            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('From group', function () {
            describe('New group', function () {
                it("Should not be executed", function (done) {
                    send_message(handler, "/list", 987654321, -147258369)
                        .then(_ => empty_queue())
                        .then(l => assert.ok(l === 0))
                        .then(_ => done("Should have thrown error"))
                        .catch(e => {
                            done()
                        });
                });

                beforeEach(async function () {
                    await empty_queue();
                });
            });
        });
    });
});
