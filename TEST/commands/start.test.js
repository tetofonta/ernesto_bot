const faunadb = require("faunadb");
const assert = require("assert");
const {send_message, telegram_api_mock, telegram_api_mock_end, wait_message, empty_queue} = require("../bot_helper");
const msg = require("../../lib/messages");
const q = faunadb.query;

describe("/start", function () {

    let client;
    let handler;
    let mock;
    before(function () {
        handler = require("../../functions/jccbot.js").handler;
        client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});
        mock = telegram_api_mock();
    });

    after(function () {
        telegram_api_mock_end()
    });

    describe('Normal execution', function () {
        describe('New user', function () {
            it("Inits and change state to gdpr", function (done) {
                send_message(handler, "/start", 543210, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.benvenuto_guest))
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.benvenuto_gdpr))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 543210))))
                    .then(data => {
                        assert.equal(data.data.current_command, "start", "Current command should be 'start'");
                        assert.equal(data.data.current_state, "gdpr", "Next state expected 'ask_year', got" + data.data.current_stat);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            it("should save and ask for year", function (done) {
                send_message(handler, "Confermo", 543210, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.ask_year))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 543210))))
                    .then(data => {
                        assert.equal(data.data.current_command, "start", "Current command should be 'start'");
                        assert.equal(data.data.current_state, "ask_year", "Next state expected 'ask_year', got" + data.data.current_stat);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            it("saves year and change state to weight", function (done) {
                send_message(handler, "2000", 543210, 342823053)
                    .then(_ => wait_message())
                    .then(ob => {
                        assert.equal(ob.text, msg.ask_weight)
                    })
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 543210))))
                    .then(data => {
                        assert.equal(data.data.current_command, "start", "Current command should be 'start'");
                        assert.equal(data.data.current_state, "ask_weight", "Next state expected 'ask_weight', got " + data.data.current_state);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            it("saves weight and aborts", function (done) {
                send_message(handler, "66kg", 543210, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.registration_ok))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 543210))))
                    .then(data => {
                        assert.equal(data.data.current_command, undefined);
                        assert.equal(data.data.current_state, undefined);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            after(async function () {
                await client.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("user_states_by_user_id"), 543210)),
                        q.Lambda(
                            'ref',
                            q.Delete(q.Var('ref'))
                        )
                    ));

                await client.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("user_data_by_user_id"), 543210)),
                        q.Lambda(
                            'ref',
                            q.Delete(q.Var('ref'))
                        )
                    ));
            });

            before(async function () {
                try {
                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("user_states_by_user_id"), 543210)),
                            q.Lambda(
                                'ref',
                                q.Delete(q.Var('ref'))
                            )
                        ));

                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("user_data_by_user_id"), 543210)),
                            q.Lambda(
                                'ref',
                                q.Delete(q.Var('ref'))
                            )
                        ));
                } catch (e) {
                }
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('Existent admin user', function () {
            it("Inits and abort", function (done) {
                send_message(handler, "/start", 123456789, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.benvenuto_admin_registred))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, undefined);
                        assert.equal(data.data.current_state, undefined);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('Existent user', function () {
            it("Inits and abort", function (done) {
                send_message(handler, "/start", 987654321, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.benvenuto_guest_registred))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 987654321))))
                    .then(data => {
                        assert.equal(data.data.current_command, undefined);
                        assert.equal(data.data.current_state, undefined);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('From group', function () {
            describe('New group', function () {
                it("Inits, saves title and aborts", function (done) {
                    send_message(handler, "/start", 987654321, -147258369, "ChatTitle")
                        .then(_ => wait_message())
                        .then(ob => assert.equal(ob.text, msg.benvenuto_group))
                        .then(_ => client.query(q.Get(q.Match(q.Index("registered_chats_by_chat_id"), -147258369))))
                        .then(data => {
                            assert.equal(data.data.title, "ChatTitle");
                        })
                        .then(_ => done())
                        .catch(e => {
                            done(e)
                        });
                });

                beforeEach(async function () {
                    await empty_queue();
                });

                after(async function () {
                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("registered_chats_by_chat_id"), -147258369)),
                            q.Lambda(
                                'ref',
                                q.Delete(q.Var('ref'))
                            )
                        ));
                });

                before(async function () {
                    try {
                        await client.query(
                            q.Map(
                                q.Paginate(q.Match(q.Index("registered_chats_by_chat_id"), -147258369)),
                                q.Lambda(
                                    'ref',
                                    q.Delete(q.Var('ref'))
                                )
                            ));
                    } catch (e) {
                    }
                });
            });

            describe('Existent group', function () {
                it("Generic message from group, no action", function (done) {
                    send_message(handler, "tart", 987654321, -7418529630)
                        .then(_ => empty_queue())
                        .then(len => assert.ok(len === 0))
                        .then(_ => done())
                        .catch(e => {
                            done(e)
                        });
                });

                it("Group title changed", function (done) {
                    let otitle = "";
                    client.query(q.Get(q.Match(q.Index("registered_chats_by_chat_id"), -7418529630)))
                        .then(data => {
                            otitle = data.data.title;
                        })
                        .then(_ => send_message(handler, undefined, 987654321, -7418529630, "a", {new_chat_title: Math.random().toString(26).substr(2)}))
                        .then(_ => client.query(q.Get(q.Match(q.Index("registered_chats_by_chat_id"), -7418529630))))
                        .then(data => {
                            assert.ok(otitle !== data.data.title)
                        })
                        .then(_ => done())
                        .catch(e => {
                            done(e)
                        });
                });

                beforeEach(async function () {
                    await empty_queue();
                })
            });
        });
    });
});
