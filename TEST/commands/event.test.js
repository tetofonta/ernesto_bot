const faunadb = require("faunadb");
const assert = require("assert");
const {send_message, telegram_api_mock, telegram_api_mock_end, wait_message, empty_queue, send} = require("../bot_helper");
const msg = require("../../lib/messages");
const q = faunadb.query;

describe("/event", function () {

    let client;
    let handler;
    let mock;
    before(function () {
        handler = require("../../functions/jccbot.js").handler;
        client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});
        mock = telegram_api_mock();
    });

    after(function () {
        telegram_api_mock_end()
    });

    describe('Normal execution', function () {

        describe('Existent admin user', function () {

            it("should change to title state", function (done) {
                send_message(handler, "/event", 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => assert.equal(m.text, msg.event_introduction))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, "event");
                        assert.equal(data.data.current_state, "get_title");
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            it("should save the title and ask for description", function (done) {
                send_message(handler, "Questo é un evento di test", 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => assert.equal(m.text, msg.event_get_description))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, "event");
                        assert.equal(data.data.current_state, "get_description");
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            it("should save the description and ask for a date", function (done) {
                send_message(handler, "Descrizione dell'evento", 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => assert.equal(m.text, msg.event_get_date))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, "event");
                        assert.equal(data.data.current_state, "get_date");
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            let chat = "";
            it("should save the date and ask for the chat", function (done) {
                send_message(handler, "27/11/2020 12:16", 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => {
                        chat = JSON.parse(m.reply_markup).keyboard[0][0].text;
                        assert.ok(typeof chat === "string");
                        assert.equal(m.text, msg.event_get_chat)
                    })
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, "event");
                        assert.equal(data.data.current_state, "select_publishing_chat");
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            it("should save the chat, show a preview and ask for confirmation", function (done) {
                send_message(handler, chat, 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => {
                        assert.equal(m.text, msg.event_make_preview(msg.event_make("Questo é un evento di test", "Descrizione dell'evento", "Fri, 27 Nov 2020 12:16:00 GMT", "0")))
                    })
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, "event");
                        assert.equal(data.data.current_state, "confirm");
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            it("should publish the event and save it in the db", function (done) {
                send_message(handler, "Conferma", 123456789, 123456789)
                    .then(_ => wait_message())
                    .then(m => {
                        console.log(m);
                        assert.ok(m.chat_id < 0);
                        assert.equal(m.text, msg.event_make("Questo é un evento di test", "Descrizione dell'evento", "Fri, 27 Nov 2020 12:16:00 GMT", "0"))
                    })
                    .then(_ => empty_queue())
                    .then(l => assert.ok(l === 0))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 123456789))))
                    .then(data => {
                        assert.equal(data.data.current_command, undefined);
                        assert.equal(data.data.current_state, undefined);
                    })
                    .then(_ => client.query(q.Get(q.Match(q.Index("published_events_by_title"), "Questo é un evento di test"))))
                    .then(data => {
                        let exp = {
                            title: "Questo é un evento di test",
                            description: "Descrizione dell'evento",
                            date: "Fri, 27 Nov 2020 12:16:00 GMT",
                            subs: 0,
                            chat_id: -7418529630,
                            message_id: 158742693
                        };
                        Object.keys(exp).forEach(k => assert.equal(data.data[k], exp[k]))
                    })
                    .then(_ => done())
                    .catch(e => done(e));
            });

            after(async function () {

                await client.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("user_states_by_user_id"), 123456789)),
                        q.Lambda(
                            'ref',
                            q.Update(q.Var('ref'), {
                                    data: {
                                        current_state: null,
                                        current_command: null
                                    }
                                }
                            )
                        )
                    )
                );

                await client.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("published_events_by_title"), "Questo é un evento di test")),
                        q.Lambda(
                            'ref',
                            q.Delete(q.Var('ref'))
                        )
                    )
                );

            });

            before(async function () {
                try {
                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("user_states_by_user_id"), 123456789)),
                            q.Lambda(
                                'ref',
                                q.Update(q.Var('ref'), {
                                        data: {
                                            current_state: null,
                                            current_command: null
                                        }
                                    }
                                )
                            )
                        ));

                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("published_events_by_title"), "Questo é un evento di test")),
                            q.Lambda(
                                'ref',
                                q.Delete(q.Var('ref'))
                            )
                        ));
                } catch (e) {
                }
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('Existent user', function () {
            it("Inits and abort", function (done) {
                send_message(handler, "/event", 987654321, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.error_not_admin))
                    .then(_ => client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 987654321))))
                    .then(data => {
                        assert.equal(data.data.current_command, undefined);
                        assert.equal(data.data.current_state, undefined);
                    })
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('From group', function () {
            describe('New group', function () {
                it("says nothing", function (done) {
                    send_message(handler, "/event", 987654321, -147258369, "ChatTitle")
                        .then(_ => empty_queue())
                        .then(l => assert.ok(l === 0))
                        .then(_ => done("Should have thrown error"))
                        .catch(e => {
                            done()
                        });
                });

                beforeEach(async function () {
                    await empty_queue();
                })
            });
        });
    })
    ;

    describe('callback', function () {
        describe('normal execution', function () {
            it("should change the message and answer", function (done) {
                send(handler, {
                    callback_query: {
                        id: '1833295657122519803',
                        from:
                            {
                                id: 987654321,
                                is_bot: false,
                                first_name: 'Stefano',
                                last_name: 'Fontana',
                                username: 'tetofonta',
                                language_code: 'en'
                            },
                        message:
                            {
                                message_id: 682,
                                from:
                                    {
                                        id: 824493855,
                                        is_bot: true,
                                        first_name: 'ErnestoCapelletti',
                                        username: 'judo_club_capelletti_bot'
                                    },
                                chat:
                                    {
                                        id: -7418529630,
                                        title: 'qaaaa',
                                        type: 'group',
                                        all_members_are_administrators: true
                                    },
                                date: 1572035752,
                                text: '--------------------\n| Abbiamo un nuovo eventolo per voi plebei!\n|AAAAAAAA\n|QQQQQQQ\n|\n|Sat, 12 Dec 2020 14:19:00 GMT\n|Attualmente 0 iscritti\n--------------------',
                            },
                        chat_instance: '-369590405737566752',
                        data: '{"action":"sub","event_title":"ThisIsASmallEvent"}'
                    }
                })
                    .then(_ => wait_message())
                    .then(m => {
                        assert.ok(m.chat_id < 0);
                        assert.equal(m.message_id, 682)
                    })
                    .then(_ => wait_message())
                    .then(m => {
                        assert.equal(m.callback_query_id, 1833295657122519803);
                        assert.equal(m.text, msg.cb_ok);
                    })
                    .then(_ => done())
                    .catch(e => done(e))
            });

            after(async function () {
                await client.query(
                    q.Map(
                        q.Paginate(q.Match(q.Index("event_subs_by_user_id"), 987654321)),
                        q.Lambda(
                            'ref',
                            q.Delete(q.Var('ref'))
                        )
                    ));
            });

            before(async function () {
                try {
                    await client.query(
                        q.Map(
                            q.Paginate(q.Match(q.Index("event_subs_by_user_id"), 987654321)),
                            q.Lambda(
                                'ref',
                                q.Delete(q.Var('ref'))
                            )
                        ));
                } catch (e) {
                }
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('not registred', function () {
            it("should answer", function (done) {
                send(handler, {
                    callback_query: {
                        id: '1833295657122519803',
                        from:
                            {
                                id: 543210,
                                is_bot: false,
                                first_name: 'Stefano',
                                last_name: 'Fontana',
                                username: 'tetofonta',
                                language_code: 'en'
                            },
                        message:
                            {
                                message_id: 682,
                                from:
                                    {
                                        id: 824493855,
                                        is_bot: true,
                                        first_name: 'ErnestoCapelletti',
                                        username: 'judo_club_capelletti_bot'
                                    },
                                chat:
                                    {
                                        id: -7418529630,
                                        title: 'qaaaa',
                                        type: 'group',
                                        all_members_are_administrators: true
                                    },
                                date: 1572035752,
                                text: '--------------------\n| Abbiamo un nuovo eventolo per voi plebei!\n|AAAAAAAA\n|QQQQQQQ\n|\n|Sat, 12 Dec 2020 14:19:00 GMT\n|Attualmente 0 iscritti\n--------------------',
                            },
                        chat_instance: '-369590405737566752',
                        data: '{"action":"sub","event_id":""}'
                    }
                })
                    .then(wait_message)
                    .then(m => {
                        assert.equal(m.callback_query_id, 1833295657122519803);
                        assert.equal(m.text, msg.cb_not_registred);
                    })
                    .then(_ => done())
                    .catch(e => done(e))
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });
    })
})
;
