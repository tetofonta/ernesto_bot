const faunadb = require("faunadb");
const assert = require("assert");
const {send_message, telegram_api_mock, telegram_api_mock_end, wait_message, empty_queue} = require("../bot_helper");
const msg = require("../../lib/messages");
const q = faunadb.query;

describe("/purge", function () {

    let client;
    let handler;
    let mock;
    before(function () {
        handler = require("../../functions/jccbot.js").handler;
        client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});
        mock = telegram_api_mock();
    });

    after(function () {
        telegram_api_mock_end()
    });

    describe('Normal execution', function () {
        describe('Existent user', function () {
            it("Should ask for confirmation", function (done) {
                send_message(handler, "/purge", 8520, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.purge_sure))
                    .then(_ => done())
                    .catch(e => {
                        done(e)
                    });
            });

            it("should have deleted everything", function (done) {
                send_message(handler, "Conferma", 8520, 342823053)
                    .then(_ => wait_message())
                    .then(ob => assert.equal(ob.text, msg.purge_ok))
                    .then(_ => {
                        client.query(q.Get(q.Match(q.Index("user_states_by_user_id"), 8520)))
                            .then(e => {
                                throw Error(e)
                            })
                            .catch(e => {
                                client.query(q.Get(q.Match(q.Index("user_date_by_user_id"), 8520)))
                                    .then(e => {
                                        throw Error(e)
                                    })
                                    .catch(e => {
                                        client.query(q.Get(q.Match(q.Index("event_subs_by_user_id"), 8520)))
                                            .then(e => {
                                                throw Error(e)
                                            })
                                            .catch(e => {
                                            })
                                    })
                            })
                    })
                    .then(_ => done(e))
                    .catch(e => {
                        done() //todo REALLY BAD
                    });
            });

            before(async function () {
                try {
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_states_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_data_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("event_subs_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                } catch (e) {
                }
                await client.query(q.Create(q.Collection("user_states"), {
                    data: {
                        id: 8520,
                        state_data: "{}"
                    }
                }));
                await client.query(q.Create(q.Collection("user_data"), {
                    data: {
                        id: 8520,
                        year: "0",
                        weight: "-1"
                    }
                }));
                await client.query(q.Create(q.Collection("event_subs"), {
                    data: {
                        user_id: 8520,
                        event_title: "aa",
                        user_name: "Gianpietro"
                    }
                }));
            });

            after(async function () {
                try {
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_states_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("user_data_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                    await db.query(q.Map(q.Paginate(q.Match(q.Index("event_subs_by_user_id"), 8520)), q.Lambda('ref', q.Delete(q.Var('ref')))));
                } catch (e) {
                }
            });

            beforeEach(async function () {
                await empty_queue();
            })
        });

        describe('From group', function () {
            describe('New group', function () {
                it("Should not be executed", function (done) {
                    send_message(handler, "/purge", 987654321, -147258369)
                        .then(_ => empty_queue())
                        .then(l => assert.ok(l === 0))
                        .then(_ => done("Should have thrown error"))
                        .catch(e => {
                            done()
                        });
                });

                beforeEach(async function () {
                    await empty_queue();
                });
            });
        });
    });
});
