const faunadb = require("faunadb");

describe("Bot", function () {

    let client;
    let handler;
    before(function () {
        handler = require("../functions/jccbot.js").handler;
        client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});
    });

    it("Should fail due to no sender", function (done) {
        handler({
                body: JSON.stringify({
                    message: {
                        text: "ciao",
                    }
                })
            },
            {},
            (e) => {
                if (!e) throw Error("Error expected");
                done()
            })
    });

    describe("Commands", function(){

        require("./commands/start.test");
        require("./commands/event.test");
        require("./commands/purge.test");
        require("./commands/list.test")

    })

});