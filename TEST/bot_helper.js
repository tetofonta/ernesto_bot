const https = require('https');
const fs = require('fs');
const path = require('path');

module.exports.send_message = function (handler, text, from, chat, title, optional) {
    return new Promise((r, ee) => handler({
        body: JSON.stringify({
            message: Object.assign( {
                text: text,
                from: {
                    id: from
                },
                chat: {
                    id: chat,
                    title
                }
            }, optional)
        })
    }, {}, (e, a) => {
        if (e) ee(e);
        r();
    }))
};

module.exports.send = function (handler, obj) {
    return new Promise((r, ee) => handler({
        body: JSON.stringify(obj)
    }, {}, (e, a) => {
        if (e) ee(e);
        r();
    }))
};

let srv;
let messages = [];

module.exports.telegram_api_mock = function () {
    process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
    srv = https.createServer({
        key: fs.readFileSync(path.resolve(__dirname, "./key.pem")),
        cert: fs.readFileSync(path.resolve(__dirname, "./cert.pem"))
    }, function (req, res) {
        let data = "";
        req.on("data", (e) => data += e.toString());

        req.on("end", () =>  {
            console.log(data);
            try{
                messages.push(JSON.parse(data))
            } catch (e) {
                console.log(data);
                messages.push(JSON.parse(data + '}'))
            }
        });

        res.writeHead(200);
        res.write("{\"message_id\": 158742693}");
        res.end();
    });
    srv.listen(4433)
};

module.exports.telegram_api_mock_end = function () {
    srv.close()
};

module.exports.wait_message = function(){
    return new Promise(async (resolve) => {
        while(!messages[0]) await new Promise(async (r) => setTimeout(r, 50));
        let a = messages.shift();
        resolve(a);
    })
};

module.exports.empty_queue = async function(){
    await new Promise(async (r) => setTimeout(r, 100));
    let l = messages.length;
    messages = [];
    return l;
};
