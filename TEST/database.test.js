const faunadb = require("faunadb");
const q = faunadb.query;

const structure = require("../db_structure");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

describe("Database", function () {
    before(async function () {

        Object.assign(process.env, require("../env_keys"));
        const client = new faunadb.Client({secret: process.env.FAUNA_DB_KEY});

        await Promise.all(Object.keys(structure.collections).map(async (e) => {
            try {
                console.log("deleting collection", e);
                await client.query(q.Delete(q.Collection(e)));
            } catch (e) {
                console.log("FAILED", e)
            }
            try {
                await sleep(61000);
                console.log("creating collection", e);
                await client.query(q.CreateCollection({name: e}));
            } catch (e) {
                console.log("FAILED", e)
            }

            // await Promise.all(structure.collections[e].map(async (d) => {
            //     await client.query(q.Create(q.Collection(e), {data: d}))
            // }));
        }));

        await Promise.all(Object.keys(structure.indexes).map(async (e) => {
            console.log("creating index", e);
            try {
                await client.query(
                    await q.CreateIndex(
                        {
                            name: e,
                            source: q.Collection(structure.indexes[e].source),
                            terms: structure.indexes[e].terms,
                            unique: !!structure.indexes[e].unique
                        }));
            } catch (e) {
                console.log("FAILED", e)
            }
        }));
    });

    it("database rebuild", function (done) {
        done()
    });

});