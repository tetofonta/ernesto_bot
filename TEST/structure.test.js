const assert = require("assert");

describe("Structure", function(){
    let handler;
    before(function(){
        handler = require("../functions/jccbot.js").handler;
    });

    it("Should export handler", function () {
        assert.ok(!!handler);
        assert.equal(typeof handler, "function")
    });

    it("Should return exception on wrong body", function (done) {
        handler(
            {
                body: "thisisnotajson"
            },
            {},
            (error, data) => {
                assert.equal(data.statusCode, 500);
                assert.ok(error.constructor, Error);
                done();
            }
        )
    });
});